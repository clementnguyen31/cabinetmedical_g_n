<?php
session_start();
include 'verif.php';
if (isLogged()) {
} else {
	header('Location: connexion.php');
}
//Connexion a la bdd
include 'database.php';

//On recupere l'ID
$id_medecin = $_POST['id_medecin'];


$req = $linkpdo->prepare("UPDATE Medecin SET civilite = :civilite , nom = :nom , prenom = :prenom WHERE id_medecin=$id_medecin");

$req->execute(array(
	'civilite' => $_POST['civilite'],
	'nom' => $_POST['nom'],
	'prenom' => $_POST['prenom']
));

echo '<script type="text/javascript">';
echo 'window.location.href = "affichagemedecin.php";';
echo '</script>';

?>