<?php
session_start();
include 'verif.php';
if (isLogged()) {
} else {
	header('Location: connexion.php');
}
include 'database';
?>

<!DOCTYPE html>
<html>

<head>
	<title>Ajout Médecin</title>
	<link rel="stylesheet" type="text/css" href="css/style-saisie.css">
	<link rel="stylesheet" type="text/css" href="css/style-nav.css">
	<link rel="stylesheet" type="text/css" href="css/style-footer.css">
	<link rel="shortcut icon" type="image/png" href="img/favicon.png">
	<style>
		img[alt="www.000webhost.com"] {
			display: none
		}
	</style>
	<meta charset="utf-8">
</head>

<body>

	<!-- Ajout de la barre de navigation + titre -->
	<?php
	include 'navbar.html';
	?>

	<div class="titre">
		<img src="img/doctor.png" />
		<h2>Ajoutez un médecin</h2>
	</div>

	<!-- Affichage du formulaire -->
	<div class="contenu">
		<form action="" method="POST">
			<input type="text" name="nom" placeholder="Nom">
			<input type="text" name="prenom" placeholder="Prénom">

			<div>
				<input type="radio" name="civilite" checked="yes" value="M."> Homme
				<input type="radio" name="civilite" value="Mme."> Femme
			</div>

			<div class="bouton">
				<input type="submit" name="ajouter" value="Ajouter">
				<input type="reset" name="reset" value="Effacer">
				<input type="button" name="afficher" value="Afficher les médecins" onclick="window.location='affichagemedecin.php'">
			</div>
		</form>
	</div>

	<!-- Ajout du footer -->
	<?php
	include 'footer.html';
	?>

</body>


</html>

<?php
if (isset($_POST['ajouter'])) {

	//Verification champs non vide et non nul
	if (isset($_POST['civilite']) && ($_POST['civilite'] != "") && isset($_POST['nom']) && ($_POST['nom'] != "") && isset($_POST['prenom']) && ($_POST['prenom'] != "")) {

		//Connexion au serveur MySQL
		include 'database.php';

		//Préparation de la requête qui permet d'inserer les donnees dans la table Medecin

		$req = $linkpdo->prepare('INSERT INTO Medecin(civilite, nom, prenom) VALUES(:civilite, :nom, :prenom)');

		///Exécution de la requête
		$req->execute(array(
			'civilite' => $_POST['civilite'],
			'nom' => $_POST['nom'],
			'prenom' => $_POST['prenom']
		));
	} else { ?>
		<script>
			alert("<?php echo 'Veuillez saisir tous les champs'; ?>")
		</script>
<?php }
}

?>