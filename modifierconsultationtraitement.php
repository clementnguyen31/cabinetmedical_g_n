<?php
session_start();
include 'verif.php';
if (isLogged()) {
} else {
	header('Location: connexion.php');
}
//Connexion a la bdd
include 'database.php';

//On recupere la date et l'horaire
$dater = $_POST['dater'];
$heured = $_POST['heured'];
$id_medecin_old = $_POST['ref_id_medecin'];

//On update puis on redirige vers la page affichage des consultations
$req = $linkpdo->prepare("UPDATE Rdv 
                        SET dater = :dater , heured = :heured , duree = :duree , id_medecin = :id_medecin , id_patient = :id_patient 
                        WHERE dater='$dater' AND heured='$heured' AND id_medecin=$id_medecin_old");

$req->execute(array(
    'dater' => $_POST['dater'],
    'heured' => $_POST['heured'],
    'duree' => $_POST['duree'],
    'id_medecin' => $_POST['id_medecin'],
    'id_patient' => $_POST['id_patient']
));

//Redirection
echo '<script type="text/javascript">';
echo 'window.location.href = "affichageconsultation.php";';
echo '</script>';

?>
