<?php
session_start();
include 'verif.php';
if (isLogged()) {
} else {
	header('Location: connexion.php');
}
//Connexion au serveur MySQL
include 'database.php';

//Requete qui selectionne tout les donnees de la table Medecin
$req = $linkpdo->prepare("SELECT * FROM Medecin");
$req->execute();

?>

<!DOCTYPE html>
<html>

<head>
	<title>Liste Médecins</title>
	<link rel="stylesheet" type="text/css" href="css/style-afficher.css">
	<link rel="stylesheet" type="text/css" href="css/style-nav.css">
	<link rel="stylesheet" type="text/css" href="css/style-footer.css">
	<style>
		img[alt="www.000webhost.com"] {
			display: none
		}
	</style>
	<link rel="shortcut icon" type="image/png" href="img/favicon.png">
	<meta charset="utf-8">
</head>

<body>

	<!-- ajout de la barre de navigation -->
	<?php
	include 'navbar.html';
	?>

	<!-- titre de la page et icone -->
	<div class="titre">
		<img src="img/doctor.png" />
		<h2>Liste des médecins</h2>
	</div>

	<!-- contenu de la page-->
	<div class="contenu">
		<!-- creation d'un tableau -->
		<table class="content-table">
			<thead>
				<tr>
					<td align="center">Civilité</td>
					<td align="center">Nom</td>
					<td align="center">Prénom</td>
					<td align="center">Supprimer</td>
					<td align="center">Modifier</td>
				</tr>
			</thead>
			<tbody>
				<!-- Parcours des données et affichage dans le tableau -->
				<?php while ($donnee = $req->fetch()) { ?>
					<tr>
						<td>
							<?php echo $donnee['civilite'] ?>
						</td>
						<td>
							<?php echo $donnee['nom'] ?>
						</td>
						<td>
							<?php echo $donnee['prenom'] ?>
						</td>
						<!-- Bouton supprimer et modifier -->
						<td><a href='supprimermedecin.php?id_medecin="<?php echo $donnee['id_medecin'] ?>"' onclick="return confirm('Êtes-vous sûr de vouloir supprimer ce patient ?');"><img class="delete-img" src="img/delete.png" /></a></td>
						<td><a href='modifiermedecin.php?id_medecin="<?php echo $donnee['id_medecin'] ?>"'><img class="delete-img" src="img/update.png" /></a></td>
					</tr>
			</tbody>
		<?php } ?>
		</table>
		<input type="button" name="afficher" value="Ajouter un médecin" onclick="window.location='ajoutermedecin.php'">
	</div>

	<!-- Ajout du footer -->
	<?php
	include 'footer.html';
	?>

</body>

</html>