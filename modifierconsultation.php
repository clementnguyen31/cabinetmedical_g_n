<?php
session_start();
include 'verif.php';
if (isLogged()) {
} else {
	header('Location: connexion.php');
}
//Connexion a la bdd
include 'database.php';

//On recupere la date et l'heure
$dater = $_GET['dater'];
$heured = $_GET['heured'];
$id_medecin = $_GET['id_medecin'];

//Requete qui selectionne toutes les donnees de Rdv qui correspond a la bonne date et horaire
$req = $linkpdo->prepare("SELECT * FROM Rdv WHERE dater=$dater AND heured=$heured AND id_medecin=$id_medecin");
$req->execute();

//Parcours
$donnee = $req->fetch();

?>

<!DOCTYPE html>
<html>

<head>
	<title>Modifier Consultation</title>
	<link rel="stylesheet" type="text/css" href="css/style-saisie.css">
	<link rel="stylesheet" type="text/css" href="css/style-nav.css">
	<link rel="stylesheet" type="text/css" href="css/style-footer.css">
	<link rel="shortcut icon" type="image/png" href="img/favicon.png">
	<style>
		img[alt="www.000webhost.com"] {
			display: none
		}
	</style>
	<meta charset="utf-8">
</head>

<body>

	<!-- Ajout de la barre de navigation + titre -->
	<?php
	include 'navbar.html';
	?>

	<div class="titre">
		<img src="img/rdv.png" />
		<h2>Modifier une consultation</h2>
	</div>

	<!-- Formulaire deja rempli -->
	<div class="contenu">
		<form action="modifierconsultationtraitement.php" method="POST">

			<!-- Selection patient -->
			<select name="id_patient">
				<?php
				$requete = $linkpdo->prepare('SELECT id_patient, nom, prenom FROM Patient');
				$requete->execute();

				while ($row = $requete->fetch()) {
					$res = "<option value=" . $row['id_patient'];
					if ($row['id_patient'] == $donnee['id_patient']) {
						$res .= " selected ";
					}

					$res .= ">" . $row['nom'] . " " . $row['prenom'] . "</option>";
					echo $res;
				}
				?>
			</select>
			<!-- Selection medecin -->
			<select name="id_medecin">
				<?php
				$requete = $linkpdo->prepare('SELECT id_medecin, nom, prenom FROM Medecin');
				$requete->execute();

				while ($row = $requete->fetch()) {
					$res = "<option value=" . $row['id_medecin'];
					if ($row['id_medecin'] == $donnee['id_medecin']) {
						$res .= " selected ";
					}

					$res .= ">" . $row['nom'] . " " . $row['prenom'] . "</option>";
					echo $res;
				}

				?>
			</select>

			<?php
			$req = $linkpdo->prepare("SELECT * FROM Rdv WHERE dater=$dater AND heured=$heured");
			$req->execute();
			$donnee = $req->fetch();
			?>

			<input type="date" name="dater" value="<?php echo $donnee['dater']; ?>">
			<input type="time" name="heured" value="<?php echo $donnee['heured']; ?>">
			<input type="text" name="duree" value="<?php echo $donnee['duree']; ?>">
			<input type="hidden" name="ref_id_medecin" value=<?php echo $donnee['id_medecin']; ?>>

			<div class="bouton">
				<input type="button" name="retour" value="Retour" onclick=window.location.href='affichageconsultation.php'>
				<input type="reset" name="reset" value="Effacer">
				<input type="submit" name="modifier" value="Modifier">
			</div>
		</form>
	</div>

	<!-- Ajout du footer -->
	<?php
	include 'footer.html';
	?>

</body>

</html>