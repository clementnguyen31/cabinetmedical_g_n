<!-- Quelques fonctions -->

<?php

//Date US en FR
setlocale(LC_TIME, 'fr_FR', 'fra');
date_default_timezone_set("Europe/Paris");
mb_internal_encoding("UTF-8");

//Fonction convertion en date francaise
function dateFr($date)
{
	return strftime('%d-%m-%Y', strtotime($date));
}

//Fonction qui enleve les secondes dans une heure
function heureminute($heure)
{
	// $heure de la forme hh:mm:ss
	// on enleve les secondes
	$arrayheure = explode(':', $heure);
	$newheure = $arrayheure[0] . ':' . $arrayheure[1];
	return $newheure; // de la forme hh:mm
}
?>