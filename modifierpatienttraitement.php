<?php
session_start();
include 'verif.php';
if (isLogged()) {
} else {
	header('Location: connexion.php');
}
//Connexion a la bdd
include 'database.php';

//On recupere l'ID
$id_patient = $_POST['id_patient'];

//On update puis on redirige vers la page affichage des patients
$req = $linkpdo->prepare("UPDATE Patient SET civilite = :civilite , nom = :nom , prenom = :prenom, adresse = :adresse, cp = :cp, ville= :ville, dateN = :dateN, lieuN = :lieuN, numSS = :numSS, id_medecin = :id_medecin WHERE id_patient=$id_patient");

$req->execute(array(
	'civilite' => $_POST['civilite'],
	'nom' => $_POST['nom'],
	'prenom' => $_POST['prenom'],
	'adresse' => $_POST['adresse'],
	'cp' => $_POST['cp'],
	'ville' => $_POST['ville'],
	'dateN' => $_POST['dateN'],
	'lieuN' => $_POST['lieuN'],
	'numSS' => $_POST['numSS'],
	'id_medecin' => $_POST['id_medecin']
));

echo '<script type="text/javascript">';
echo 'window.location.href = "affichagepatient.php";';
echo '</script>';
?>