<?php
session_start();
require 'verif.php';
if (isLogged()) {
} else {
	header('Location: connexion.php');
}
?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/style-nav.css">
	<link rel="stylesheet" type="text/css" href="css/style-index.css">
	<link rel="stylesheet" type="text/css" href="css/style-footer.css">
	<link rel="shortcut icon" type="image/png" href="img/favicon.png">
	<style>
		img[alt="www.000webhost.com"] {
			display: none
		}
	</style>
	<title>Bienvenue</title>
</head>

<body>

	<!-- Ajout de la barre de navigation -->
	<?php
	include 'navbar.html';
	?>

	<!-- 4 menus avec des icones -->
	<div class="contenu">
		<div class="haut">
			<div class="patient">
				<a href="ajouterpatient.php"><img src="img/patient.png" /></a>
				<input type="button" name="patient" value="Patients" onclick="window.location='affichagepatient.php'">
			</div>
			<div class="consultation">
				<a href="ajouterconsultation.php"><img src="img/rdv.png" /></a>
				<input type="button" name="consultation" value="Consultations" onclick="window.location='affichageconsultation.php'">
			</div>
		</div>
		<div class="bas">
			<div class="medecin">
				<a href="ajoutermedecin.php"><img src="img/doctor.png" /></a>
				<input type="button" name="medecin" value="Médecins" onclick="window.location='affichagemedecin.php'">
			</div>
			<div class="stats">
				<a href="stats.php"><img src="img/stats.png" /></a>
				<input type="button" name="stats" value="Statistiques" onclick="window.location='stats.php'">
			</div>
		</div>
	</div>

	</div>

	<!-- Ajout du footer -->
	<?php
	include 'footer.html';
	?>

</body>

</html>