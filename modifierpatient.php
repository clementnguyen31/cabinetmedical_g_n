<?php
session_start();
include 'verif.php';
if (isLogged()) {
} else {
	header('Location: connexion.php');
}
//Connexion a la bdd
include 'database.php';
//On recupere l'ID
$id_patient = $_GET['id_patient'];

//Requete qui selectionne toutes les donnees de Patient qui correspond a la bonne ID
$req = $linkpdo->prepare("SELECT * FROM Patient WHERE id_patient=$id_patient");
$req->execute();
$donnee = $req->fetch();

?>


<!DOCTYPE html>
<html>

<head>
	<title>Modifier Patient</title>
	<link rel="stylesheet" type="text/css" href="css/style-saisie.css">
	<link rel="stylesheet" type="text/css" href="css/style-nav.css">
	<link rel="stylesheet" type="text/css" href="css/style-footer.css">
	<link rel="shortcut icon" type="image/png" href="img/favicon.png">
	<style>
		img[alt="www.000webhost.com"] {
			display: none
		}
	</style>
	<meta charset="utf-8">
</head>

<body>

	<!-- Ajout de la barre de navigation + titre -->
	<?php
	include 'navbar.html';
	?>

	<div class="titre">
		<img src="img/patient.png" />
		<h2>Modifier un patient</h2>
	</div>

	<!-- Formulaire deja rempli -->
	<div class="contenu">
		<form action="modifierpatienttraitement.php" method="POST">
			<input type="hidden" name="id_patient" value="<?php echo $donnee['id_patient']; ?>">
			<input type="text" name="nom" value="<?php echo $donnee['nom']; ?>">
			<input type="text" name="prenom" value="<?php echo $donnee['prenom']; ?>">
			<input type="text" name="adresse" value="<?php echo $donnee['adresse']; ?>">
			<input type="text" name="cp" value="<?php echo $donnee['cp']; ?>">
			<input type="text" name="ville" value="<?php echo $donnee['ville']; ?>">
			<input type="date" name="dateN" value="<?php echo $donnee['dateN']; ?>">
			<input type="text" name="lieuN" value="<?php echo $donnee['lieuN']; ?>">
			<input type="text" name="numSS" value="<?php echo $donnee['numSS']; ?>">
			<select name="id_medecin">
				<option value="0">
					<?php
					if ($donnee['id_medecin'] <> 0) {
						$req2 = 'SELECT * FROM Medecin WHERE id_medecin = ' . $donnee['id_medecin'];
						$rep = $linkpdo->prepare($req2);
						$rep->execute();
						$row = $rep->fetch();
						echo $row['nom'] . " " . $row['prenom'];
					} else {
						echo "Pas de medecin référent";
					}
					?></option>
				<?php
				$req = $linkpdo->prepare("SELECT * FROM Medecin WHERE id_medecin <> " . $donnee['id_medecin']);
				$req->execute();
				while ($donnee = $req->fetch()) {
					echo "<option value=\"" . $donnee['id_medecin'] . "\">" . $donnee['nom'] . " " . $donnee['prenom'] . "</option>";
				}
				?>

			</select>

			<div>
				<?php $req = $linkpdo->prepare("SELECT * FROM Patient WHERE id_patient=$id_patient");
				$ok = $req->execute();
				$donnee = $req->fetch(); ?>
				<input type="radio" name="civilite" value="M." <?php if ($donnee['civilite'] == 'M.') echo "checked=\"checked\""; ?>> Homme
				<input type="radio" name="civilite" value="Mme." <?php if ($donnee['civilite'] == 'Mme.') echo "checked=\"checked\""; ?>> Femme
			</div>

			<div class="bouton">
				<input type="button" name="retour" value="Retour" onclick=window.location.href='affichagepatient.php'>
				<input type="reset" name="reset" value="Effacer">
				<input type="submit" name="modifier" value="Modifier">
			</div>
		</form>
	</div>

	<!-- Ajout du footer -->
	<?php
	include 'footer.html';
	?>

</body>

</html>