<?php
//Ouverture d'une session
session_start();
//Connexion a la bdd
include 'database.php';
if (isset($_POST["login"])) {
	//Verification champs username et password non vide
	if (empty($_POST["username"]) || empty($_POST["passwd"])) { ?>
		<script>
			alert("<?php echo 'Veuillez saisir tous les champs'; ?>")
		</script>
		<?php } else {
		//Requete qui selectionne les utilisateurs dont l'username et le mdp correspond
		$req = "SELECT * FROM user WHERE username = :username AND passwd = :passwd";
		$rep = $linkpdo->prepare($req);
		$rep->execute(
			array(
				'username' => $_POST['username'],
				'passwd' => $_POST['passwd']
			)
		);
		//On compte le nombre de ligne
		$count = $rep->rowCount();
		//Si c'est OK, on stock dans la variable de SESSION et on redirige vers l'accueil
		if ($count > 0) {
			$_SESSION['username'] = $_POST['username'];
			echo '<script type="text/javascript">';
			echo 'window.location.href = "accueil.php";';
			echo '</script>';
		} else { ?>
			<script>
				alert("<?php echo "Nom d'utilisateur ou mot de passe incorrect "; ?>")
			</script>
<?php }
	}
}
?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/style-login.css">
	<link rel="stylesheet" href="css/style-footer.css">
	<link rel="shortcut icon" type="image/png" href="img/favicon.png">
	<style>
		img[alt="www.000webhost.com"] {
			display: none
		}
	</style>
	<title>Connexion</title>
</head>

<body>

	<!-- Formulaire -->
	<div class="login-wrapper">
		<form autocomplete="off" action="" class="form" method="POST">
			<h1>Cabinet Medical</h1>
			<div class="input-group">
				<input type="text" name="username">
				<label for="user">Nom d'utilisateur</label>
			</div>
			<div class="input-group">
				<input type="password" name="passwd">
				<label for="password">Mot de passe</label>
			</div>
			<input type="submit" name="login" value="Connexion" class="submit-btn">
		</form>
	</div>

	<!-- Ajout du footer -->
	<?php
	include "footer.html";
	?>

</body>

</html>