<?php
session_start();
include 'verif.php';
if (isLogged()) {
} else {
	header('Location: connexion.php');
}
//Connexion au serveur MySQL
include 'database.php';
//Pour utiliser les fonctions dans le fichier fonctions.php
include 'fonctions.php';

//Requete qui selectionne tout les donnees de la table Rdv trier par date
$id_medecin = $_POST['id_medecin'];
$req = $linkpdo->prepare("SELECT * FROM Rdv WHERE id_medecin = $id_medecin ORDER BY dater DESC");
$req->execute();

?>

<!DOCTYPE html>
<html>

<head>
    <title>Liste consultations</title>
    <link rel="stylesheet" type="text/css" href="css/style-afficher.css">
    <link rel="stylesheet" type="text/css" href="css/style-nav.css">
    <link rel="stylesheet" type="text/css" href="css/style-footer.css">
    <link rel="stylesheet" type="text/css" href="css/style-filtre.css">
    <style>
        img[alt="www.000webhost.com"] {
            display: none
        }
    </style>
    <link rel="shortcut icon" type="image/png" href="img/favicon.png">
    <meta charset="utf-8">
</head>

<body>

    <!-- ajout de la barre de navigation -->
    <?php
    include 'navbar.html';
    ?>

    <!-- titre de la page et icone -->
    <div class="titre">
        <img src="img/rdv.png" />
        <h2>Liste des consultations</h2>
    </div>

    <!-- contenu de la page-->
    <div class="contenu">

        <!-- Filtrage des consultations par medecin -->
        <form action="affichageconsultationfiltre.php" id="monFormulaire" method="POST">
            <div class="selec-filtrage">
                <select name="id_medecin" onchange="document.getElementById('monFormulaire').submit();">
                    <?php
                    //On affiche le nom et prenom du medecin que l'on a voulu filtrer
                    $req2 = $linkpdo->prepare("SELECT * FROM Medecin WHERE id_medecin = $id_medecin");
                    $req2->execute();
                    $donnee = $req2->fetch();
                    ?>
                    <option value="0"><?php echo $donnee['nom'] . " " . $donnee['prenom']; ?></option>
                    <option value="All">Tous les médecins</option>
                    <?php
                    //Tout les medecins
                    if ($_POST['id_medecin'] == 'All') {
                        //Redirection
                        echo '<script type="text/javascript">';
                        echo 'window.location.href = "affichageconsultation.php";';
                        echo '</script>';
                    }
                    //Les autres medecins
                    $req2 = $linkpdo->prepare("SELECT * FROM Medecin WHERE id_medecin != $id_medecin");
                    $req2->execute();
                    while ($donnee = $req2->fetch()) {
                        echo "<option value=\"" . $donnee['id_medecin'] . "\">" . $donnee['nom'] . " " . $donnee['prenom'] . "</option>";
                    }
                    ?>
                </select>
            </div>
        </form>

        <!-- creation d'un tableau -->
        <table class="content-table">
            <thead>
                <tr>
                    <td align="center">Patient</td>
                    <td align="center">Médecin</td>
                    <td align="center">Date</td>
                    <td align="center">Horaire</td>
                    <td align="center">Durée</td>
                    <td align="center">Supprimer</td>
                    <td align="center">Modifier</td>
                </tr>
            </thead>
            <tbody>
                <!-- Parcours de la requete -->
                <?php while ($donnee = $req->fetch()) { ?>
                    <tr>
                        <td>
                            <?php
                            //affichage du nom et du prenom du patient
                            if ($donnee['id_patient'] <> 0) {
                                //Requete qui selectionne le patient dont l'id correspond a celle de l'id patient de la table rdv
                                $req2 = 'SELECT * FROM Patient WHERE id_patient = ' . $donnee['id_patient'];
                                $rep = $linkpdo->prepare($req2);
                                $rep->execute();
                                $row = $rep->fetch();
                                echo $row['nom'] . " " . $row['prenom'];
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            //affichage du nom et du prenom du medecin
                            if ($donnee['id_medecin'] <> 0) {
                                //Requete qui selectionne le medecin dont l'id correspond a celle de l'id medecin de la table rdv
                                $req2 = 'SELECT * FROM Medecin WHERE id_medecin = ' . $donnee['id_medecin'];
                                $rep = $linkpdo->prepare($req2);
                                $rep->execute();
                                $row = $rep->fetch();
                                echo $row['nom'] . " " . $row['prenom'];
                            }
                            ?>
                        </td>
                        <!-- Affichage de la date, de l'heure et de la duree -->
                        <td>
                            <?php echo dateFr($donnee['dater']) ?>
                        </td>
                        <td>
                            <?php echo heureminute($donnee['heured']) ?>
                        </td>
                        <td>
                            <?php echo $donnee['duree'] ?>
                        </td>
                        <!-- Bouton supprimer et modifier -->
                        <td><a href='supprimerconsultation.php?dater="<?php echo $donnee['dater'] ?>"&heured="<?php echo $donnee['heured'] ?>"' onclick="return confirm('Êtes-vous sûr de vouloir supprimer ce patient ?');"><img class="delete-img" src="img/delete.png" /></a></td>
                        <td><a href='modifierconsultation.php?dater="<?php echo $donnee['dater'] ?>"&heured="<?php echo $donnee['heured'] ?>"'><img class="delete-img" src="img/update.png" /></a></td>
                    </tr>
            </tbody>
        <?php } ?>
        </table>
        <input type="button" name="afficher" value="Ajouter une consultation" onclick="window.location='ajouterconsultation.php'">
    </div>



    <!-- Ajout du footer -->
    <?php
    include 'footer.html';
    ?>

</body>

</html>