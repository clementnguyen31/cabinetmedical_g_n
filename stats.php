<?php
session_start();
include 'verif.php';
if (isLogged()) {
} else {
	header('Location: connexion.php');
}
//Connexion au serveur MySQL
include 'database.php';
//Requete qui selectionne la somme des durees qu'a effectuer les medecins
$req = $linkpdo->prepare("SELECT id_medecin, sum(duree) as total FROM Rdv GROUP BY id_medecin");
$req->execute();

?>
<!DOCTYPE html>
<html>

<head>
	<title>Statistiques</title>
	<link rel="stylesheet" type="text/css" href="css/style-stats.css">
	<link rel="stylesheet" type="text/css" href="css/style-nav.css">
	<link rel="stylesheet" type="text/css" href="css/style-footer.css">
	<link rel="shortcut icon" type="image/png" href="img/favicon.png">
	<style>
		img[alt="www.000webhost.com"] {
			display: none
		}
	</style>
	<meta charset="utf-8">
</head>

<body>

	<!-- Ajout de la barre de navigation + titre -->
	<?php
	include 'navbar.html';
	?>

	<div class="titre">
		<img src="img/stats.png" />
		<h2>Les statistiques</h2>
	</div>

	<!-- Sous titre : Statistique NB HEURE CONSULTATION -->
	<p class="soustitre">Durée totale des consultations effectuées par chaque médecin</p>

	<div class="contenu">
		<table class="content-table">
			<thead>
				<tr>
					<th>Médecin</th>
					<th>Durée (h)</th>
				</tr>
			</thead>
			<tbody>
				<?php while ($donnee = $req->fetch()) { ?>
					<tr>
						<td>
							<?php
							//Affichage de nom et prenom du medecin
							$req2 = $linkpdo->prepare('SELECT * FROM Medecin, Rdv WHERE Medecin.id_medecin = ' . $donnee['id_medecin']);
							$req2->execute();
							$row = $req2->fetch();
							echo $row['nom'] . " " . $row['prenom'];
							?>
						</td>
						<td>
							<!-- Convertion en heure -->
							<?php echo $donnee['total'] / 60 ?>
						</td>
					</tr>
			</tbody>
		<?php } ?>
		</table>
	</div>

	<!-- Sous titre : Statistique AGE DES PATIENTS -->
	<p class="soustitre">Répartition des patients selon leur sexe et leur âge </p>

	<div class="contenu">
		<table>
			<tr>
				<th>Tranche d'âge</th>
				<th>Nombre d'hommes</th>
				<th>Nombre de femmes</th>
			</tr>
			<tr>
				<th>Moins de 25 ans</th>
				<td>
					<!-- Tout les patients masculins de moins de 25 ans-->
					<?php $req = "SELECT count(*) as total 
							  FROM Patient 
							  WHERE civilite='M.'
							  AND datediff(NOW(),dateN) < 365*25;";
					$rep = $linkpdo->prepare($req);
					$rep->execute();
					$row = $rep->fetch();
					echo $row['total'] ?>
				</td>
				<td>
					<!-- Tout les patients feminins de moins de 25 ans-->
					<?php $req = "SELECT count(*) as total 
							  FROM Patient 
							  WHERE civilite='Mme.'
							  AND datediff(NOW(),dateN) < 365*25;";
					$rep = $linkpdo->prepare($req);
					$rep->execute();
					$row = $rep->fetch();
					echo $row['total'] ?></td>
			</tr>
			<tr>
				<th>Entre 25 et 50 ans</th>
				<td>
					<!-- Tout les patients masculins entre 25 et 50 ans -->
					<?php $req = "SELECT count(*) as total 
							  FROM Patient 
							  WHERE civilite='M.'
							  AND datediff(NOW(),dateN) BETWEEN 365*25 AND 365*50;";
					$rep = $linkpdo->prepare($req);
					$rep->execute();
					$row = $rep->fetch();
					echo $row['total'] ?></td>
				<td>
					<!-- Tout les patients feminins entre 25 et 50 ans -->
					<?php $req = "SELECT count(*) as total 
							  FROM Patient 
							  WHERE civilite='Mme.'
							  AND datediff(NOW(),dateN) BETWEEN 365*25 AND 365*50;";
					$rep = $linkpdo->prepare($req);
					$rep->execute();
					$row = $rep->fetch();
					echo $row['total'] ?></td>
			</tr>
			<tr>
				<th>Plus de 50 ans</th>
				<td>
					<!-- Tout les patients masculins de plus de 50ans -->
					<?php $req = "SELECT count(*) as total 
							  FROM Patient 
							  WHERE civilite='M.'
							  AND datediff(NOW(),dateN) > 365*50;";
					$rep = $linkpdo->prepare($req);
					$rep->execute();
					$row = $rep->fetch();
					echo $row['total'] ?></td>
				<td>
					<!-- Tout les patients feminins de plus de 50ans -->
					<?php $req = "SELECT count(*) as total 
							  FROM Patient 
							  WHERE civilite='Mme.'
							  AND datediff(NOW(),dateN) > 365*50;";
					$rep = $linkpdo->prepare($req);
					$rep->execute();
					$row = $rep->fetch();
					echo $row['total'] ?></td>
			</tr>
		</table>
	</div>

	<!-- Ajout du footer-->
	<?php
	include 'footer.html';
	?>


</body>

</html>