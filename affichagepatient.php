<?php
session_start();
include 'verif.php';
if (isLogged()) {
} else {
	header('Location: connexion.php');
}
//Connexion au serveur MySQL
include 'database.php';
//Pour utiliser les fonctions dans le fichier fonctions.php
include 'fonctions.php';

//Requete qui selectionne tout les donnees de la table Patient trier par l'id
$req = $linkpdo->prepare("SELECT * FROM Patient ORDER BY id_patient");
$req->execute();

?>

<!DOCTYPE html>
<html>

<head>
	<title>Liste patients</title>
	<link rel="stylesheet" type="text/css" href="css/style-afficher.css">
	<link rel="stylesheet" type="text/css" href="css/style-nav.css">
	<link rel="stylesheet" type="text/css" href="css/style-footer.css">
	<link rel="shortcut icon" type="image/png" href="img/favicon.png">
	<style>
		img[alt="www.000webhost.com"] {
			display: none
		}
	</style>
	<meta charset="utf-8">
</head>

<body>

	<!-- ajout de la barre de navigation -->
	<?php
	include 'navbar.html';
	?>

	<!-- titre de la page et icone -->
	<div class="titre">
		<img src="img/patient.png" />
		<h2>Liste des patients</h2>
	</div>

	<!-- contenu de la page-->
	<div class="contenu">
		<table class="content-table">
			<thead>
				<tr>
					<td align="center">Civilité</td>
					<td align="center">Nom</td>
					<td align="center">Prénom</td>
					<td align="center">Adresse</td>
					<td align="center">Code Postal</td>
					<td align="center">Ville</td>
					<td align="center">Date de naissance</td>
					<td align="center">Lieu de naissance</td>
					<td align="center">N° sécurité social</td>
					<td align="center">Médecin réferent</td>
					<td align="center">Supprimer</td>
					<td align="center">Modifier</td>
				</tr>
			</thead>
			<tbody>
				<!-- Parcours des données et affichage dans le tableau -->
				<?php while ($donnee = $req->fetch()) { ?>
					<tr>
						<td>
							<?php echo $donnee['civilite'] ?>
						</td>
						<td>
							<?php echo $donnee['nom'] ?>
						</td>
						<td>
							<?php echo $donnee['prenom'] ?>
						</td>
						<td>
							<?php echo $donnee['adresse'] ?>
						</td>
						<td>
							<?php echo $donnee['cp'] ?>
						</td>
						<td>
							<?php echo $donnee['ville'] ?>
						</td>
						<td>
							<?php echo dateFr($donnee['dateN']) ?>
						</td>
						<td>
							<?php echo $donnee['lieuN'] ?>
						</td>
						<td>
							<?php echo $donnee['numSS'] ?>
						</td>
						<td>
							<!-- Affichage du nom et prenom du medecin referent -->
							<?php
							if ($donnee['id_medecin'] <> 0) {
								//Requete qui selectionne le medecin dont l'id correspond a celle de l'id medecin de la table Patient
								$req2 = 'SELECT * FROM Medecin WHERE id_medecin = ' . $donnee['id_medecin'];
								$rep = $linkpdo->prepare($req2);
								$rep->execute();
								$row = $rep->fetch();
								echo $row['nom'] . " " . $row['prenom'];
							} else {
								echo "Pas de medecin référent";
							}
							?>
						</td>
						<!-- Bouton supprimer et modifier -->
						<td><a href='supprimerpatient.php?id_patient="<?php echo $donnee['id_patient'] ?>"' onclick="return confirm('Êtes-vous sûr de vouloir supprimer ce patient ?');"><img class="delete-img" src="img/delete.png" /></a></td>
						<td><a href='modifierpatient.php?id_patient="<?php echo $donnee['id_patient'] ?>"'><img class="delete-img" src="img/update.png" /></a></td>
					</tr>
			</tbody>
		<?php } ?>
		</table>
		<input type="button" name="afficher" value="Ajouter un patient" onclick="window.location='ajouterpatient.php'">
	</div>

	<!-- Ajout du footer -->
	<?php
	include 'footer.html';
	?>

</body>

</html>