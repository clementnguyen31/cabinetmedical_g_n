<?php
session_start();
include 'verif.php';
if (isLogged()) {
} else {
	header('Location: connexion.php');
}
//Connexion a la bdd
include 'database.php';

//On recupere l'ID
$id_medecin = $_GET['id_medecin'];

//Requete qui selectionne toutes les donnees de Medecin qui correspond a la bonne ID
$req = $linkpdo->prepare("SELECT * FROM Medecin WHERE id_medecin=$id_medecin");
$ok = $req->execute();
$donnee = $req->fetch();

?>


<!DOCTYPE html>
<html>

<head>
	<title>Modifier Médecin</title>
	<link rel="stylesheet" type="text/css" href="css/style-saisie.css">
	<link rel="stylesheet" type="text/css" href="css/style-nav.css">
	<link rel="stylesheet" type="text/css" href="css/style-footer.css">
	<style>
		img[alt="www.000webhost.com"] {
			display: none
		}
	</style>
	<link rel="shortcut icon" type="image/png" href="img/favicon.png">
	<meta charset="utf-8">
</head>

<body>

	<!-- Ajout de la barre de navigation + titre -->
	<?php
	include 'navbar.html';
	?>

	<div class="titre">
		<img src="img/doctor.png" />
		<h2>Modifier un médecin</h2>
	</div>

	<!-- Formulaire deja rempli -->
	<div class="contenu">
		<form action="modifiermedecintraitement.php" method="POST">
			<input type="hidden" name="id_medecin" value="<?php echo $donnee['id_medecin']; ?>">
			<input type="text" name="nom" value="<?php echo $donnee['nom']; ?>">
			<input type="text" name="prenom" value="<?php echo $donnee['prenom']; ?>">

			<div>
				<?php $req = $linkpdo->prepare("SELECT * FROM Medecin WHERE id_medecin=$id_medecin");
				$ok = $req->execute();
				$donnee = $req->fetch(); ?>
				<input type="radio" name="civilite" value="M." <?php if ($donnee['civilite'] == 'M.') echo "checked=\"checked\""; ?>> Homme
				<input type="radio" name="civilite" value="Mme." <?php if ($donnee['civilite'] == 'Mme.') echo "checked=\"checked\""; ?>> Femme
			</div>

			<div class="bouton">
				<input type="button" name="retour" value="Retour" onclick=window.location.href='affichagemedecin.php'>
				<input type="reset" name="reset" value="Effacer">
				<input type="submit" name="modifier" value="Modifier">
			</div>
		</form>
	</div>

	<!-- Ajout du footer -->
	<?php
	include 'footer.html';
	?>

</body>

</html>