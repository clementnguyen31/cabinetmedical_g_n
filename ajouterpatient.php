<?php
session_start();
include 'verif.php';
if (isLogged()) {
} else {
	header('Location: connexion.php');
}
include 'database.php';
?>

<!DOCTYPE html>
<html>

<head>
	<title>Ajout Patient</title>
	<link rel="stylesheet" type="text/css" href="css/style-saisie.css">
	<link rel="stylesheet" type="text/css" href="css/style-nav.css">
	<link rel="stylesheet" type="text/css" href="css/style-footer.css">
	<link rel="shortcut icon" type="image/png" href="img/favicon.png">
	<style>
		img[alt="www.000webhost.com"] {
			display: none
		}
	</style>
	<meta charset="utf-8">
</head>

<body>

	<!-- Ajout de la barre de navigation + titre -->
	<?php
	include 'navbar.html';
	?>

	<div class="titre">
		<img src="img/patient.png" />
		<h2>Ajoutez un patient</h2>
	</div>

	<!-- Affichage du formulaire -->
	<div class="contenu">
		<form action="" method="POST">
			<input type="text" name="nom" placeholder="Nom">
			<input type="text" name="prenom" placeholder="Prénom">
			<input type="text" name="adresse" placeholder="Adresse">
			<input type="text" name="cp" placeholder="Code postal">
			<input type="text" name="ville" placeholder="Ville">
			<input type="date" name="dateN" placeholder="Date de naissance">
			<input type="text" name="lieuN" placeholder="Lieu de naissance">
			<input type="text" name="numSS" placeholder="N° sécurité social">

			<!-- Bouton selection pour choisir un medecin referant -->
			<select name="id_medecin">
				<option value="0">Pas de médecin référant</option>
				<?php
				//On parcours toutes les donnees et on affiche le nom et prenom des medecins
				$req = $linkpdo->prepare("SELECT * FROM Medecin");
				$req->execute();
				while ($donnee = $req->fetch()) {
					echo "<option value=\"" . $donnee['id_medecin'] . "\">" . $donnee['nom'] . " " . $donnee['prenom'] . "</option>";
				}
				?>

			</select>

			<div>
				<input type="radio" name="civilite" checked="yes" value="M."> Homme
				<input type="radio" name="civilite" value="Mme."> Femme
			</div>

			<div class="bouton">
				<input type="submit" name="ajouter" value="Ajouter">
				<input type="reset" name="reset" value="Effacer">
				<input type="button" name="afficher" value="Afficher les patients" onclick="window.location='affichagepatient.php'">
			</div>
		</form>
	</div>

	<!-- Ajout du footer -->
	<?php
	include 'footer.html';
	?>

</body>

</html>

<?php

if (isset($_POST['ajouter'])) {

	//Verification champs non vide et non nul
	if (isset($_POST['civilite']) && ($_POST['civilite'] != "") && isset($_POST['nom']) && ($_POST['nom'] != "") && isset($_POST['prenom']) && ($_POST['prenom'] != "") && isset($_POST['adresse']) && ($_POST['adresse'] != "") && isset($_POST['cp']) && ($_POST['cp'] != "") && isset($_POST['ville']) && ($_POST['ville'] != "") && isset($_POST['dateN']) && ($_POST['dateN'] != "") && isset($_POST['lieuN']) && ($_POST['lieuN'] != "") && isset($_POST['numSS']) && ($_POST['numSS'] != "") && isset($_POST['id_medecin']) && ($_POST['id_medecin'] != "")) {

		//Préparation de la requête qui permet d'inserer les donnees dans la table Patient
		$req = $linkpdo->prepare('INSERT INTO Patient(civilite, nom, prenom, adresse, cp, ville, dateN,
		lieuN, numSS, id_medecin) VALUES(:civilite, :nom, :prenom, :adresse, :cp, :ville, :dateN, :lieuN, :numSS, :id_medecin)');

		///Exécution de la requête
		$req->execute(array(
			'civilite' => $_POST['civilite'],
			'nom' => $_POST['nom'],
			'prenom' => $_POST['prenom'],
			'adresse' => $_POST['adresse'],
			'cp' => $_POST['cp'],
			'ville' => $_POST['ville'],
			'dateN' => $_POST['dateN'],
			'lieuN' => $_POST['lieuN'],
			'numSS' => $_POST['numSS'],
			'id_medecin' => $_POST['id_medecin']
		));
	} else { ?>
		<script>
			alert("<?php echo 'Veuillez saisir tous les champs'; ?>")
		</script>
<?php }
}
?>