<?php
session_start();
include 'verif.php';
if (isLogged()) {
} else {
	header('Location: connexion.php');
}
include 'database.php';

?>

<?php
if (!isset($_POST['nb'])) {
	$nb = 0;
} else {
	$nb = $_POST['nb'];
}

if (!empty($_POST['id_patient'])) {
	$reqRef = $linkpdo->prepare("SELECT id_medecin FROM Patient WHERE id_patient=" . $_POST['id_patient']);
	$reqRef->execute();
	$idMedecinSelected = $reqRef->fetchAll()[0]['id_medecin'];
}

if (isset($_POST['ajouter'])) {

	//Verification champs non vide et non nul
	if (isset($_POST['id_patient']) && ($_POST['id_patient'] != "") && isset($_POST['id_medecin']) && ($_POST['id_medecin'] != "") && isset($_POST['dater']) && ($_POST['dater'] != "") && isset($_POST['heured']) && ($_POST['heured'] != "") && isset($_POST['duree']) && ($_POST['duree'] != "")) {
		//Non chevauchement : on verifie si l'horaire est disponible : cas ou l'heure de début est durant une autre consultation et cas ou l'heure de fin est durant une autre consultation
		$verif = $linkpdo->prepare('SELECT count(*) FROM Rdv WHERE id_medecin = :id_medecin AND dater = :dater AND (:heured BETWEEN heured AND ADDTIME(heured,sec_to_time(duree * 60)) OR ADDTIME(:heured,sec_to_time(:duree*60)) BETWEEN heured AND ADDTIME(heured,sec_to_time(duree * 60)))');
		$verif->execute(array(
			'id_medecin' => $_POST['id_medecin'],
			'dater' => $_POST['dater'],
			'heured' => $_POST['heured'],
			'duree' => $_POST['duree']
		));
		$res = $verif->fetch();
		if ($res['count(*)'] > 0) { ?>
			<script>
				alert("<?php echo 'Horaire non disponible'; ?>")
			</script>
		<?php
		} else {
			//Horaire disponible
			//Préparation de la requête qui permet d'inserer les donnees dans la table Rdv
			$req = $linkpdo->prepare('INSERT INTO Rdv(id_patient, id_medecin, dater, heured, duree) VALUES(:id_patient, :id_medecin, :dater, :heured, :duree)');

			///Exécution de la requête
			$req->execute(array(
				'id_patient' => $_POST['id_patient'],
				'id_medecin' => $_POST['id_medecin'],
				'dater' => $_POST['dater'],
				'heured' => $_POST['heured'],
				'duree' => $_POST['duree']
			));
		}
	} else { ?>
		<script>
			alert("<?php echo 'Veuillez saisir tous les champs'; ?>")
		</script>
<?php }
}
?>

<!DOCTYPE html>
<html>

<head>
	<title>Ajout consultation</title>
	<link rel="stylesheet" type="text/css" href="css/style-saisie.css">
	<link rel="stylesheet" type="text/css" href="css/style-nav.css">
	<link rel="stylesheet" type="text/css" href="css/style-footer.css">
	<link rel="shortcut icon" type="image/png" href="img/favicon.png">
	<style>
		img[alt="www.000webhost.com"] {
			display: none
		}
	</style>
	<meta charset="utf-8">
</head>

<body>

	<!-- Ajout de la barre de navigation + titre -->
	<?php
	include 'navbar.html';
	?>

	<div class="titre">
		<img src="img/rdv.png" />
		<h2>Ajoutez une consultation</h2>
	</div>

	<!-- Contenu de la page -->
	<div class="contenu">
		<form action="" method="POST">
			<!-- Bouton selection pour choisir un patient + medecin par default si il existe un medecin referant -->
			<select onchange="this.form.submit()" name="id_patient">
				<?php
				if ($nb === 0) {
					echo "<option selected>Selectionnez un patient</option>";
					$nb += 1;
				}
				$requete = $linkpdo->prepare('SELECT * FROM Patient');
				$requete->execute();

				while ($donnee = $requete->fetch()) {
					if (!empty($_POST['id_patient']) && $_POST['id_patient'] == $donnee['id_patient'] && $nb > 0) {
						echo "<option selected value=" . $donnee['id_patient'] . ">" . $donnee['nom'] . " " . $donnee['prenom'] . "</option>";
					} else {
						echo "<option value=" . $donnee['id_patient'] . ">" . $donnee['nom'] . " " . $donnee['prenom'] . "</option>";
					}
				}

				?>
			</select>

			<select name="id_medecin">
				<?php
				$requete = $linkpdo->prepare('SELECT * FROM Medecin');
				$requete->execute();

				if ($_POST['id_patient'])
					while ($donnee = $requete->fetch()) {
						if (!empty($_POST['id_patient']) && $idMedecinSelected == $donnee['id_medecin']) {
							echo "<option selected value=" . $donnee['id_medecin'] . ">" . $donnee['nom'] . " " . $donnee['prenom'] . "</option>";
						} else {
							echo "<option value=" . $donnee['id_medecin'] . ">" . $donnee['nom'] . " " . $donnee['prenom'] . "</option>";
						}
					}

				?>
			</select>

			<p>Date<input type="date" name="dater" placeholder="Date"></p>
			<p>Horaire<input type="time" name="heured" placeholder="Heure" min="10:00" max="18:00" value="10:00"></p>
			<p>Durée (min)<input type="text" name="duree" placeholder="Durée" value="30"></p>
			<input type="hidden" name="nb" value=<?php echo $nb; ?>>

			<input type="submit" name="ajouter" value="Ajouter">
			<input type="reset" name="reset" value="Effacer">
			<input type="button" name="afficher" value="Afficher les consultations" onclick="window.location='affichageconsultation.php'">
		</form>
	</div>

	<!-- Ajout du footer-->
	<?php
	include 'footer.html';
	?>

</body>

</html>