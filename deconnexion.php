<?php
session_start();
require 'verif.php';
$_SESSION = array();

//On detruit la session puis on redirige vers la page de connexion
session_destroy();
if (isLogged()) {
} else {
	header('Location: connexion.php');
}
?>
